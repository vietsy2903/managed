<?php

$templatepath = get_template_directory();
define('T_THEME', get_template_directory_uri());
add_filter( 'use_block_editor_for_post', '__return_false' );
add_filter( 'auto_update_plugin', '__return_false' );


add_filter( 'rest_user_query' , 'all_users' );
function all_users( $prepared_args, $request = null ) {
  	unset($prepared_args['has_published_posts']);
  	return $prepared_args;
}
//////////////////////////////////////////////////////////////////////////////////
// Custom themes
//////////////////////////////////////////////////////////////////////////////////
function custom_theme_setup() {
	add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'custom_theme_setup');

//////////////////////////////////////////////////////////////////////////////////
// Field ACF
//////////////////////////////////////////////////////////////////////////////////
require $templatepath.'/function/acf.php';

//////////////////////////////////////////////////////////////////////////////////
// Custom Post Type
//////////////////////////////////////////////////////////////////////////////////
require $templatepath.'/function/custom_post_type.php';

//////////////////////////////////////////////////////////////////////////////////
// User
//////////////////////////////////////////////////////////////////////////////////
require $templatepath.'/function/user.php';

//////////////////////////////////////////////////////////////////////////////////
// Tasks
//////////////////////////////////////////////////////////////////////////////////
require $templatepath.'/function/tasks.php';


//////////////////////////////////////////////////////////////////////////////////
// Rest API
//////////////////////////////////////////////////////////////////////////////////

/* Sent mail
------------------------------------------------------------------------------- */
// add_action( 'rest_api_init', function () {
//   register_rest_route( 'contact/v2', '/add', array(
//     'methods' => 'POST',
//     'callback' => 'contact_func',
//   ));
// });

// function contact_func($data) {
// 	$post_id = wp_insert_post(array(
// 		//'ID' => $data['ID'],
// 		'post_title'		=> $data['name'],
// 		'post_type'			=> 'contact',		
// 	));

// 	//update_field(the_field('email'), $data['name'], $post_id );

// 	if($post_id > 0 && !is_wp_error($post_id)) {
// 		header("Access-Control-Allow-Origin: *");
// 		$mail = new PHPMailer(true);

// 		try {
// 			//Server settings
// 			$mail->SMTPDebug = SMTP::DEBUG_SERVER;
// 			$mail->isSMTP();
// 			$mail->Host       = 'smtp.gmail.com';
// 			$mail->SMTPAuth   = true;
// 			$mail->Username   = 'syvv@icd-vn.com';
// 			$mail->Password   = 'wgyzbietvxlbyyzm';
// 			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
// 			$mail->Port       = 587;

// 			//Recipients
// 			$mail->setFrom('syvv@icd-vn.com', $data['Name']);
// 			$mail->addAddress('syvv@icd-vn.com');
// 			$mail->addAddress($data['email']);

// 			$body= '';
// 			if(isset($data['name'])) {
// 				$body .= 'Name:'.$data['name'];
// 			}
// 			if(isset($data['email'])) {
// 				$body .= 'Email:'.$data['email'];
// 			}
// 			if(isset($data['tel'])) {
// 				$body .= 'Email:'.$data['email'];
// 			}
// 			//Content
// 			$mail->isHTML(true);
// 			$mail->Subject = $data['name'].'-'.$data['email'];
// 			$mail->Body    =  $body;

// 			$mail->send();
// 			return 'Message has been sent';
// 		} catch (Exception $e) {
// 			return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
// 		}	
// 	}
// }

/* Add field
------------------------------------------------------------------------------- */
// Add various fields to the JSON output
function api_register_fields() {

	//register fields post type Tasks
	register_rest_field( 'tasks', 'task_date',
		array(
			'get_callback'    => function() {
				return get_the_date('d/m/Y');
			},
			'update_callback' => null,
			'schema'          => null,
		)
	);
	register_rest_field( 'tasks', 'categories',
		array(
			'get_callback'    => 'get_category_task',
			'update_callback' => null,
			'schema'          => null,
		)
	);
}
add_action( 'rest_api_init', 'api_register_fields' );

function api_get_image_src( $object, $field_name, $request ) {
	if($object['featured_media'] == 0) {
		return $object['featured_media'];
	}
	$feat_img_array = wp_get_attachment_image_src($object['featured_media'], 'full', true);
	return $feat_img_array[0];
}

function get_category_task($object){
	$post_categories = array();
	$categories = wp_get_post_terms( $object['id'], 'tasks_cat', array('fields'=>'all') );
	foreach ($categories as $term) {
		$term_link = get_term_link($term);
		if ( is_wp_error( $term_link ) ) {
			continue;
		}
		$post_categories[] = array('term_id'=>$term->term_id, 'name'=>$term->name, 'link'=>$term_link);
	}
	return $post_categories;
}