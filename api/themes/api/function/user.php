<?php
add_action( 'rest_api_init', function () {
    register_rest_route( 'users/', '/all', array(
        'methods'       => WP_REST_Server::READABLE,
        'callback'      => 'all_user',
        'show_in_rest'  => true
    ));

    register_rest_route( 'users/', '/login', array(
        'methods'       => 'POST',
        'callback'      => 'login',
    ));
});

function all_user() {
    $results = get_users();
    $users = array();
    $controller = new WP_REST_Users_Controller();
    foreach ( $results as $user ) {
        $data    = $controller->prepare_item_for_response( $user, $request );
        $users[] = $controller->prepare_response_for_collection( $data );
    }     
    return rest_ensure_response( $users );
}

function login($request){
    $creds = array();
    $creds['user_login'] = $request["username"];
    $creds['user_password'] =  $request["password"];
    $creds['remember'] = true;

    $user = wp_signon( $creds, false );

    if ( !is_wp_error($user)) { 
        wp_set_current_user($user->ID);
        wp_set_auth_cookie( $user->ID, true, true );       
    } else {
        //echo $user->get_error_message();
        return 'getUserFromValidToken failed!';
    }

    if ($user) {
        wp_set_current_user($user->ID);
        wp_set_auth_cookie($user->ID);        
        exit();
    } else {
        return 'getUserFromValidToken failed!';
    }

    //return $user->ID;

    // if ( is_wp_error($user) )
    //   echo $user->get_error_message();

    //return $user;
    wp_send_json( $user );
}