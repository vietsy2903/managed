<?php
add_action( 'rest_api_init', function () {
	register_rest_route( 'task/', '/add', array(
		'methods'  => 'POST',
		'callback' => 'add_task',
	) );
} );

function add_task( WP_REST_Request $req ) {
	$data = $req->get_json_params();

	$post_id = wp_insert_post( array(
		'post_title'   => $data['valueInput']['taskname'],
		'post_content' => $data['valueInput']['taskcontent'],
		'post_author'  => 1, // Lấy user id đang login
		'post_status'  => 'publish',
		'post_type'    => 'tasks',
	) );

	if ( is_numeric( $post_id ) && $post_id > 0 ) {
		update_field( 'field_progress', '0', $post_id );
		update_field( 'field_status', 'open', $post_id );
		update_field( 'field_rank', $data['valueInput']['rank'], $post_id );
		update_field( 'field_start_date', $data['startDate'], $post_id );
		update_field( 'field_end_date', $data['endDate'], $post_id );
		update_field( 'field_member', array_values($data['valueMember']['member']), $post_id );

		$listTodos     = isset( $data['listTodos'] ) && is_array( $data['listTodos'] ) ? $data['listTodos'] : array();
		$listTodosData = array();
		foreach ( $listTodos as $todo ) {
			if ( ! empty( $todo['dataList'] ) ) {
				$listTodosData[] = array(
					'field_check' => 0,
					'field_label' => $todo['dataList']
				);
			}
		}
		if ( count( $listTodosData ) ) {
			update_field( 'field_list_todo', $listTodosData, $post_id );
		}

		wp_send_json_success( [ "task_id" => $post_id ] );
	}

	wp_send_json_error();
}