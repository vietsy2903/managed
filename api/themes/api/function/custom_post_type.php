<?php

/* Taks
------------------------------------------------------------------------------- */
function create_tasks() {
	register_post_type( 'tasks',
		array(
			'labels' 			  	=> array(
				'name' 			  	=> __( 'Task' ),
				'singular_name'   	=> __( 'Tasks' ),
				'menu_name'       	=> __('Task'),
			),
			'public'              	=> true,
			'publicly_queryable'  	=> true,
			'has_archive'         	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'query_var'           	=> true,
			'map_meta_cap'        	=> true,
			'hierarchical'        	=> false,
			'menu_position'       	=> 4,
			'rewrite'             	=> array( 'slug' => 'tasks' ),
			'supports' 			  	=> array('title','editor','thumbnail','comments','author'),
			'show_in_rest'        	=> true,
			'rest_base'           	=> 'tasks',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	register_taxonomy( 'tasks_cat', 'land', array(
		'labels' 				=> array(
			'name' 				=> _x( 'Categories', 'land' ),
			'singular_name' 	=> _x( 'Category', 'land' ),
			'all_items' 		=> __( 'All Categories' ),
			'parent_item' 		=> null,
			'parent_item_colon' => null,
			'edit_item' 		=> __( 'Edit Category' ), 
			'update_item' 		=> __( 'Update Category' ),
			'add_new_item' 		=> __( 'Add New Category' ),
			'new_item_name' 	=> __( 'New Category Name' ),
			'menu_name' 		=> __( 'Categories' ),
		),
		'hierarchical' 			=> true,
		'show_admin_column' 	=> true,
		'query_var'         	=> true,
		'rewrite'           	=> array( 'slug' => 'tasks_cat' ),
		'show_ui'           	=> true,
		'show_in_rest'      	=> true,
		'rest_base'         	=> 'tasks_cat',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	));
}
add_action( 'init', 'create_tasks' );