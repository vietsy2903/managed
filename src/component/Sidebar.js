import React from "react";
import Nav from './Nav';

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        var { menus } = this.props;
        return (
            <div className="l-sidebar">
                <Nav menus={menus} />
            </div>
        );
    }
}

export default Sidebar;