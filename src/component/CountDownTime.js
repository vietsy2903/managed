import React, { useState, useEffect } from "react";

// class CountDownTime extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             isdate: '0',
//             days: '0',
//             hours: '0',
//             mins: '0',
//             secs: '0'
//         };
//     }

//     getTime() {
//         const time = Date.parse(this.state.isdate) - Date.parse(new Date());
//         const secs = Math.floor((time / 1000) % 60);
//         const mins = Math.floor((time / 1000 / 60) % 60);
//         const hours = Math.floor((time / 1000 / 60 / 60) % 24);
//         const days = Math.floor((time / 1000 / 60 / 60 / 24));

//         if (time > 0) {
//             this.setState({ days, hours, mins, secs });
//         } else {
//             this.setState({ days: '0', hours: '0', mins: '0', secs: '0' });
//         }
//     }

//     componentDidMount() {
//         this.setState({
//             isdate: this.props.date,
//         });
//         setInterval(() => this.getTime(this.state.isdate, 1000));
//     }

//     render() {
//         const { days, hours, mins, secs } = this.state;
//         return (
//             <ul className="c-countdown">
//                 <li>{days > 9 ? days : '0' + days}<span>days</span></li>
//                 <li>{hours > 9 ? hours : '0' + hours}<span>hours</span></li>
//                 <li>{mins > 9 ? mins : '0' + mins}<span>mins</span></li>
//                 <li>{secs > 9 ? secs : '0' + secs}<span>secs</span></li>
//             </ul>
//         );
//     }
// }

function CountDownTime(props) {
    const [countDate, setCountDate] = useState({
        days: '0',
        hours: '0',
        minutes: '0',
        seconds: '0'
    });
    useEffect(() => {
        let interval = null;
        const conuntDownDate = new Date(props.date).getTime();

        interval = setInterval(() => {
            const now = new Date().getTime();
            const distance = conuntDownDate - now;
            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
            const minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
            const seconds = Math.floor(distance % (1000 * 60) / 1000);

            if (distance < 0) {
                clearInterval(interval);
            } else {
                setCountDate({
                    days: days,
                    hours: hours,
                    minutes: minutes,
                    seconds: seconds
                });
            }
        }, 1000);
        return () => {
            clearInterval(interval);
        }
    }, [props.date]);
    return (
        <ul className="c-countdown">
            <li>{countDate.days > 9 ? countDate.days : '0' + countDate.days}<span>days</span></li>
            <li>{countDate.hours > 9 ? countDate.hours : '0' + countDate.hours}<span>hours</span></li>
            <li>{countDate.minutes > 9 ? countDate.minutes : '0' + countDate.minutes}<span>mins</span></li>
            <li>{countDate.seconds > 9 ? countDate.seconds : '0' + countDate.seconds}<span>secs</span></li>
        </ul>
    );
}

export default CountDownTime;