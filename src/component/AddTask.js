import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { allUser } from '../Api';

function AddTask(props) {
    const [active, setActive] = useState(true);
    const isActive = active ? 'is-active' : '';
    const [dataMember, setDataMember] = useState([]);
    const [valueInput, setValueInput] = useState({
        rank: 'low',
        taskname: '',
        taskcontent: '',
    });
    const [valueMember, setValueMember] = useState({
        member: {}
    });
    const [listTodos, setListTodos] = useState([{ dataList: null }]);

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const Checkbox = ({ type = "checkbox", name, id, checked = false, onChange }) => {
        return (
            <input type={type} name={name} id={id} checked={checked} onChange={onChange} />
        );
    };

    const onChange = (event) => {
        //const value = event.target.type === "checkbox" ? event.target.checked : event.target.value;
        setValueInput({
            ...valueInput,
            //[event.target.name]: value,
            [event.target.name]: event.target.value,
        });
    };

    const onCheck = (event) => {
        setValueMember({
            ...valueMember,
            member: {
                ...valueMember.member,
                [event.target.name]: event.target.checked === true ? event.target.id : ''
            }
        });
    };

    const onRepeater = (i, event) => {
        const values = [...listTodos];
        values[i].dataList = event.target.value;
        setListTodos(values);
    };

    const onAdd = () => {
        const values = [...listTodos];
        values.push({ dataList: null });
        setListTodos(values);
    };

    const onRemove = (i) => {
        const values = [...listTodos];
        values.splice(i, 1);
        setListTodos(values);
    };

    const hideAddTask = () => {
        setActive(false);
        props.hideAddTask();
    }
    const onSaveTask = (event) => {
        event.preventDefault();
        setActive(false);
        props.hideAddTask();
        const data = {
            startDate,
            endDate,
            valueInput,
            valueMember,
            listTodos
        };        
        props.onSaveTask(data);
        // addTask.post(`task/add/`, data)
        //     .then(res => {
        //         console.log(res.data);
        //         if (res.statusText === 'OK') {
        //             console.log('ok');
        //         }
        //     });
    }

    useEffect(() => {
        allUser().then(res => {
            setDataMember(res.data);
        });
    }, []);

    return (
        <div className={"p-addtask " + (isActive)}>
            <div className="p-addtask__block">
                <h3 className="p-addtask__head">Add New Task</h3>
                <div className="p-addtask__body">
                    <div className="c-taskform">
                        <div className="c-taskform__row">
                            <div className="c-taskform__select">
                                <select name="rank" onChange={onChange} value={valueInput.rank} className={`is-${valueInput.rank}`}>
                                    <option value="high">High</option>
                                    <option value="medium">Medium</option>
                                    <option value="low">Low</option>
                                </select>
                                <span className={`is-focus is-${valueInput.rank}`}>Choose Rank</span>
                            </div>
                        </div>
                        <div className="c-taskform__row">
                            <div className="c-taskform__date">
                                <label className="c-taskform__input">
                                    <DatePicker className={!startDate ? '' : 'is-active'} dateFormat="dd/MM/yyyy" selected={startDate} onChange={date => setStartDate(date)} selectsStart startDate={startDate} endDate={endDate} minDate={new Date()} />
                                    <span className={!startDate ? '' : 'is-focus'}>Start Date</span>
                                </label>
                            </div>
                            <div className="c-taskform__date">
                                <label className="c-taskform__input">
                                    <DatePicker className={!endDate ? '' : 'is-active'} dateFormat="dd/MM/yyyy" selected={endDate} onChange={date => setEndDate(date)} selectsEnd startDate={startDate} endDate={endDate} minDate={startDate} />
                                    <span className={!endDate ? '' : 'is-focus'}>End Date</span>
                                </label>
                            </div>
                        </div>
                        <div className="c-taskform__row">
                            <label className="c-taskform__input">
                                <input className={!valueInput.taskname ? '' : 'is-active'} type="text" name="taskname" onChange={onChange} value={valueInput.taskname} />
                                <span className={!valueInput.taskname ? '' : 'is-focus'}>Task Name</span>
                            </label>
                        </div>
                        <div className="c-taskform__row">
                            <label className="c-taskform__select">
                                <textarea className={!valueInput.taskcontent ? '' : 'is-active'} name="taskcontent" value={valueInput.taskcontent} onChange={onChange} />
                                <span className={!valueInput.taskcontent ? '' : 'is-focus'}>Task Content</span>
                            </label>
                        </div>
                        <div className="c-taskform__row c-taskform__row--checklist">
                            <h3 className="c-taskform__ttl">List Todo</h3>
                            <ul className="c-checklist">
                                {listTodos.map((field, idx) => {
                                    return (
                                        <li key={`${field}-${idx}`}>
                                            <label className="c-taskform__input">
                                                <input className={!field.dataList ? '' : 'is-active'} type="text" name={`check_list${idx}`} value={field.dataList || ""} onChange={e => onRepeater(idx, e)} />
                                                <span className={!field.dataList ? '' : 'is-focus'}>List Todo No.{idx + 1}</span>
                                            </label>
                                            <button onClick={() => onRemove(idx)} type="button" className="btn btn-outline-success c-checklist__remove" data-mdb-ripple-color="dark"><i className="fas fa-ellipsis-h" /></button>
                                        </li>
                                    );
                                })}
                                <button onClick={onAdd} type="button" className="btn btn-outline-success" data-mdb-ripple-color="dark">Add</button>
                            </ul>
                        </div>
                        <div className="c-taskform__row">
                            <h3 className="c-taskform__ttl">Menber</h3>
                            <ul className="c-taskform__member">
                                {dataMember && dataMember.map((item, index) => (
                                    item.name === 'admin_api' ? '' : <li key={index}>
                                        <label className="ripple ripple-surface-info">
                                            <Checkbox name={item.slug} id={item.id} checked={valueMember.member[item.slug]} onChange={onCheck} />
                                            <span>
                                                <figure><img src={item.simple_local_avatar ? item.simple_local_avatar['full'] : item.avatar_urls['96']} alt={item.name} /></figure>
                                                {item.name}
                                            </span>
                                        </label>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="p-addtask__footer">
                    <button onClick={hideAddTask} type="button" className="btn btn-outline-success" data-mdb-ripple-color="dark">Cancel</button>
                    <button onClick={onSaveTask} type="button" className="btn btn-outline-success" data-mdb-ripple-color="dark">Save</button>
                </div>
            </div>
        </div>
    );
}

export default AddTask;