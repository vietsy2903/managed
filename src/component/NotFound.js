import React from 'react';

class NotFound extends React.Component {
    render() {
        return (
            <div className="l-content">
                404 - Khong tim thay trang
            </div>
        );
    }
}

export default NotFound;