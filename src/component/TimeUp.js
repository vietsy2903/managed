import React, { useState, useEffect } from "react";

function TimeUp(props) {
    const [second, setSecond] = useState("00");
    const [minute, setMinute] = useState("00");
    const [hours, setHours] = useState("00");
    const [isActive, setIsActive] = useState(false);
    const [counter, setCounter] = useState(0);

    const onStart = () => {
        setTimeout(() => {
            setIsActive(!isActive);
        }, 300);
    };
    const onStopTime = () => {
        setTimeout(() => {
            setIsActive(!isActive);
        }, 300);
        props.onStopTime(hours,minute,second);
        //console.log(hours + ':' + minute + ':' + second);
    };
    useEffect(() => {
        let intervalId;
        if (isActive) {
            intervalId = setInterval(() => {
                const secondCounter = counter % 60;
                const minuteCounter = Math.floor(counter / 60);
                const hoursCounter = Math.floor(counter / 60 / 60);

                let computedSecond = String(secondCounter).length === 1 ? `0${secondCounter}` : secondCounter;
                let computedMinute = String(minuteCounter).length === 1 ? `0${minuteCounter}` : minuteCounter;
                let computedHours = String(hoursCounter).length === 1 ? `0${hoursCounter}` : hoursCounter;

                setSecond(computedSecond);
                setMinute(computedMinute);
                setHours(computedHours);

                setCounter((counter) => counter + 1);
            }, 1000);
        }
        return () => clearInterval(intervalId);
    }, [isActive, counter]);
    return (
        <div className="c-timeup">
            <span className="c-timeup__time">{hours}</span>
            <span className="c-timeup__no">:</span>
            <span className="c-timeup__time">{minute}</span>
            <span className="c-timeup__no">:</span>
            <span className="c-timeup__time">{second}</span>
            <div className="c-timeup__btn">
                {isActive
                    ? <button onClick={onStopTime} className="btn btn-outline-success btn-rounded" data-mdb-ripple-color="dark">Stop</button>
                    : <button onClick={onStart} className="btn btn-outline-success btn-rounded" data-mdb-ripple-color="dark">Start</button>
                }
            </div>
        </div>
    );
}

export default TimeUp;