import React from "react";
import { Route, Link } from "react-router-dom";

function Nav(props) {
    const MenuLink = ({ label, to, activeOnlyWhenExact, icon }) => {
        return (
            <Route
                path={to}
                exact={activeOnlyWhenExact}
                icon={icon}
                children={({ match }) => {
                    var active = match ? 'is-active' : '';
                    return (
                        <li className={`c-nav__item ${active}`}>
                            <Link to={to}>
                                <span className="c-nav__icon">{icon}</span>
                                <span className="c-nav__name">{label}</span>
                            </Link>
                        </li>
                    );
                }}
            />
        );
    }

    const elmMenu = props.menus.map((menu, index) => {
        var icon = menu.icon === '' ? <i className="fas fa-skull" /> : <i className={menu.icon} />;
        return (
            <MenuLink
                key={index}
                label={menu.name}
                to={menu.to}
                activeOnlyWhenExact={menu.exact}
                icon={icon}
            />
            // <li key={index} className="c-nav__item">
            //     <NavLink to={menu.to} activeClassName="is-active" className="ripple ripple-surface-info" data-mdb-ripple-duration="1s">
            //         <span className="c-nav__icon">{icon}</span>
            //         <span className="c-nav__name">{menu.name}</span>
            //     </NavLink>
            // </li >
        );
    });
    return (
        <nav className="c-nav">
            <ul>
                {elmMenu}
            </ul >
        </nav>
    )
}

export default Nav;