import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import logo from '../assets/images/logo.png';

function Header(props) {
    const [displayName] = useState('Manage');
    const [avatar] = useState('/assets/images/account.png');
    const elmTitle = props.title ? <h2 className="c-header__title">{props.title}</h2> : '';
    useEffect(() => {
        // Update the document title using the browser API

    });
    return (
        <header className="c-header">
            <h1 className="c-header__logo ripple ripple-surface-light">
                <Link to="/"><img src={logo} alt="" /></Link>
            </h1>
            <div className="c-header__inner">
                {elmTitle}
                <div className="c-user">
                    <div className="c-user__avatar ripple ripple-surface-dark">
                        <img src={process.env.PUBLIC_URL + avatar} alt={displayName} />
                    </div>
                    <button type="button" className="btn btn-info" data-mdb-ripple-color="dark">Logout</button>
                </div>
            </div>
        </header>
    );
}

export default Header;