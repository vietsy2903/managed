import { React, Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }
    onHandelChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value
        });
    }
    onSubmit = (event) => {
        event.preventDefault();
        //this.props.onSubmit(this.state);
        //setUserSession("aaa", this.state);
    }
    render() {
        return (
            <div className="p-login">
                <div className="p-login__form">
                    <h3 className="p-login__title">Login</h3>
                    <div className="c-form">
                        <form onSubmit={this.onSubmit}>
                            <div className="c-form__row">
                                <input onChange={this.onHandelChange} type="text" name="username" value={this.state.username} placeholder="User Name" />
                            </div>
                            <div className="c-form__row">
                                <input onChange={this.onHandelChange} type="password" name="password" value={this.state.password} placeholder="Password" />
                            </div>
                            <div className="c-form__btn">
                                <button type="submit"><span>Login</span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;