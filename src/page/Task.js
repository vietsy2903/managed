import React, { useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import TimeUp from '../component/TimeUp';
import CountDownTime from '../component/CountDownTime';
import { getTask } from '../Api';

function Task(props) {
    const { id } = useParams();
    const [loading, setLoading] = useState(false);
    const [loadStatus, setLoadStatus] = useState(false);

    const [title, setTitle] = useState('');
    const [rank, setRank] = useState();
    const [status, setStatus] = useState();
    const [dateStart, setDateStart] = useState();
    const [dateEnd, setDateEnd] = useState();
    const [desc, setDesc] = useState();
    const [member, setMember] = useState();
    const [listTodos, setListTodos] = useState();
    const [progress, setProgress] = useState('0');

    const [valueListTodo, setValueListTodo] = useState({});

    const onChecked = (event) => {
        setValueListTodo({
            ...valueListTodo,
            [event.target.name]: event.target.checked
        });
    };
    const updateInfo = (data) => {
        setTimeout(() => {
            setLoadStatus(false);
        }, 1000);
        console.log(data);
    };
    const saveListTodo = () => {
        console.log(valueListTodo);
    };

    const onStopTime = (hours, minute, second) => {
        console.log(hours + ':' + minute + ':' + second);
    };

    useEffect(() => {
        document.title = `${title}`;
        setLoading(true);
        getTask.get(`/${id}`).then(res => {
            const task = res.data;
            setLoading(false);
            setTitle(task.title.rendered);
            setRank(task.acf.rank);
            setStatus(task.acf.status);
            setDateStart(task.acf.start_date);
            setDateEnd(task.acf.end_date);
            setDesc(task.content.rendered);
            setMember(task.acf.member);
            setListTodos(task.acf.list_todo);
            setProgress(task.acf.progress);
        }).catch(err => console.log(err));

    }, [id, title]);

    return (
        <>
            {loading ? <div>Loading...</div> : <div className="l-content">
                {/* <Prompt
                when={active === false}
                message={location => `Please Stop Time`}
                /> */}
                <div className="p-task">
                    <div className="p-task__progress">
                        <span style={{ width: progress + '%' }}></span>
                        <span></span>
                        <p>{progress + '%'}</p>
                    </div>
                    <h2 className="p-task__title">{title}</h2>
                    <div className="p-task__control">
                        <button className={`btn ${rank}`} type="button">{rank}</button>
                        <div className="dropdown dropend">
                            <button className={`btn ${status}`} type="button" data-mdb-toggle="dropdown" aria-expanded="false">{loadStatus ? 'Loading...' : status}</button>
                            <ul className={`dropdown-menu ${status}`}>
                                <li onClick={() => updateInfo({ 'status': 'process' }, setLoadStatus(true))} className="dropdown-item">Process</li>
                                <li onClick={() => updateInfo({ 'status': 'fixing' }, setLoadStatus(true))} className="dropdown-item">Fixing</li>
                                <li onClick={() => updateInfo({ 'status': 'checking' }, setLoadStatus(true))} className="dropdown-item">Checking</li>
                                <li onClick={() => updateInfo({ 'status': 'completed' }, setLoadStatus(true))} className="dropdown-item">Completed</li>
                            </ul>
                        </div>
                    </div>
                    <p className="p-task__date">
                        <span><b>Start:</b>{dateStart}</span>
                        <span><b>End:</b>{dateEnd}</span>
                    </p>
                    {dateEnd && <CountDownTime date={dateEnd} />}
                    <div className="p-task__inner">
                        <div className="p-task__block">
                            <h3 className="p-task__ttl">Time</h3>
                            <div className="p-task__time">
                                <TimeUp onStopTime={onStopTime} />
                            </div>
                        </div>
                        <div className="p-task__block">
                            <h3 className="p-task__ttl">Description</h3>
                            <div className="p-task__desc" dangerouslySetInnerHTML={{ __html: desc }}></div>
                        </div>
                        <div className="p-task__block">
                            <h3 className="p-task__ttl">List Todo</h3>
                            <div className="p-task__todo">
                                {listTodos && listTodos.map((item, index) => {
                                    return (
                                        <label className="ripple ripple-surface-info" key={index}>
                                            <input type="checkbox" name={`list_todo${index}`} checked={item.check ? item.check : valueListTodo[`list_todo${index}`] || false} disabled={item.check} onChange={onChecked} />
                                            <span>{item.label}</span>
                                        </label>
                                    );
                                })}
                            </div>
                            {Object.keys(valueListTodo).length > 0 ? <button onClick={saveListTodo} type="button" className="btn btn-outline-success btn-rounded" data-mdb-ripple-color="dark">Save</button> : ''}
                        </div>
                        <div className="p-task__block">
                            <h3 className="p-task__ttl">Member</h3>
                            <ul className="p-task__member">
                                {member && member.map(item => (
                                    <li className="ripple ripple-surface-info" key={item.ID} dangerouslySetInnerHTML={{ __html: item.user_avatar }} data-mdb-toggle="tooltip" title={item.display_name}></li>
                                ))}
                            </ul>
                        </div>
                        <div className="p-task__block p-task__block--full">
                            <h3 className="p-task__ttl">Date Work</h3>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>May 22, 2021</td>
                                        <td>00:00:00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>}

        </>
    );
}

export default Task;