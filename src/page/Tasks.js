import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import AddTask from '../component/AddTask';
import { getAllTasks, addTasks } from '../Api';

function Tasks(props) {
    const manage = props.manage;
    const [addTask, setAddTask] = useState(false);
    const [allTasks, setAllTasks] = useState();

    function showAddTask() {
        if (addTask && manage !== false) {
            setAddTask(false);
        } else {
            setAddTask(true);
        }
    }
    function hideAddTask() {
        setTimeout(() => {
            setAddTask(false);
        }, 1000);
    }

    function onSaveTask(data) {
        addTasks.post(`task/add/`, data)
            .then(res => {
                console.log(res.data);
                if (res.statusText === 'OK') {
                    console.log('ok');
                }
            });
        setTimeout(() => {
            getAllTasks().then(res => {
                setAllTasks(res.data);
            });
        }, 1000);
    }

    useEffect(() => {
        getAllTasks().then(res => {
            setAllTasks(res.data);
        });
    }, []);
    return (
        <>
            <div className="l-content">
                <div className="p-tasks">
                    <div className="p-tasks__control d-flex">
                        {manage ? <button onClick={showAddTask} type="button" className="btn btn-outline-success btn-rounded" data-mdb-ripple-color="dark">Add Task</button> : ''}
                    </div>
                    <ul className="c-tasks">
                        {allTasks && allTasks.map((item, index) => {
                            const start = new Date(item.acf.start_date);
                            const end = new Date(item.acf.end_date);
                            const diffDays = Math.round(Math.abs((end - start) / (24 * 60 * 60 * 1000)));
                            const number = 5;
                            const numberMember = item.acf.member.length > number ? number : item.acf.member.length;
                            const dataMembers = [];
                            for (let i = 0; i < numberMember; i++) {
                                const data = item.acf.member[i];
                                dataMembers.push(<span key={data.ID} data-mdb-toggle="tooltip" title={data.display_name} dangerouslySetInnerHTML={{ __html: data.user_avatar }}></span>);
                            }
                            return (
                                <li className="c-tasks__item" key={index}>
                                    <Link to={`/tasks/${item.id}`}>
                                        <span className={`c-tasks__rank is-${item.acf.rank}`}>{item.acf.rank}</span>
                                        <span className="c-tasks__status">{item.acf.status}</span>
                                        <h3 className="c-tasks__title">{item.title.rendered}</h3>
                                        <p className="c-tasks__date">{item.acf.start_date}<i className="fas fa-angle-double-right" />{item.acf.end_date}</p>
                                        <div className="c-tasks__progress">
                                            <p>Progress</p>
                                            <div className="c-tasks__bar">
                                                <span style={{ width: item.acf.progress + '%' }}></span>
                                                <span></span>
                                            </div>
                                            <p>{item.acf.progress}%</p>
                                        </div>
                                        <div className="c-tasks__footer">
                                            {item.acf.member ? <div className="c-tasks__user">
                                                {item.acf.member.length > number ? <blockquote>+</blockquote> : ''}
                                                {dataMembers}
                                            </div> : ''}
                                            <span className="c-tasks__time">{diffDays} Days Left</span>
                                        </div>
                                    </Link>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
            {addTask && manage ? <AddTask hideAddTask={hideAddTask} onSaveTask={onSaveTask} /> : ''}
        </>
    );
}

export default Tasks;