import React, { useState, useEffect } from "react";
import { Switch, Route, useLocation } from "react-router-dom";

import './assets/scss/style.scss'

import Header from './component/Header';
import Sidebar from './component/Sidebar';
import Login from './page/Login';
import Home from './page/Home';
import Tasks from './page/Tasks';
import Task from './page/Task';

import NotFound from './component/NotFound';

function App() {
    const [manage] = useState(true);
    const [title, setTitle] = useState('');
    const location = useLocation();

    const menus = [
        {
            name: 'Dashboard',
            to: '/',
            exact: true,
            icon: 'fas fa-tachometer-alt'
        }, {
            name: 'Tasks',
            to: '/tasks',
            exact: false,
            icon: 'fas fa-tasks'
        }, {
            name: 'Authors',
            to: '/authors',
            exact: false,
            icon: 'fas fa-users'
        }
    ];

    useEffect(() => {
        menus.forEach(menu => {
            if (location.pathname === menu.to) {
                setTitle(menu.name);
            }
        });
        document.title = `${title}`;
    });

    return (
        <>
            <Header title={title} />
            <Sidebar menus={menus} />
            <div className="l-main">
                <Switch>
                    <Route exact path="/login">
                        <Login />
                    </Route>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route exact path="/tasks">
                        <Tasks manage={manage} />
                    </Route>
                    <Route path="/tasks/:id" onEnter={() => console.log('Works fine')}>
                        <Task />
                    </Route>
                    <Route exact path='*' component={NotFound} />
                </Switch>
            </div>

        </>
    );
}

export default App;
