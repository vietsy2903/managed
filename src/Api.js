import axios from 'axios';
//import { useParams } from 'react-router-dom';
// export const allUser = axios.create({
//     baseURL: 'http://api.cd/wp-json/users/all',
//     headers: {
//         "Content-type": "application/json"
//     }
// });

const baseUrl = 'http://api.cd/wp-json';
//const { id } = useParams();

export const allUser = () => {
    return axios.get(baseUrl + '/wp/v2/users');
};

export const getAllTasks = () => {
    return axios.get(baseUrl + '/wp/v2/tasks/');
};

export const getTask = axios.create({
    baseURL: baseUrl + '/wp/v2/tasks/',
    headers: {
        "Content-type": "application/json"
    }
});

export const addTasks = axios.create({
    baseURL: baseUrl,
    headers: {
        "Content-type": "application/json"
    }
});