import axios from 'axios';
const baseUrl = 'http://api.cd/wp-json';
export const allUsers = () => {
    return axios.get(baseUrl + '/users/all');
};